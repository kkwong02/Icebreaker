# ----------------------------------------------------------------------------
# icebreaker.py v0.40
# CMPT 103 OP02
# Kirsten Kwong

# Program: ice breaker game. Shows menu and lets player choose a computer vs 
# player game or player vs player game. Players take turns moving and breaking
# ice until one player can't move. The first player unable to make a move loses.
# valid moves are left, right, up, down and diagnal. Any piece that is not 
# occupied by a player/the computer or is already broke can be broken. 
# ----------------------------------------------------------------------------

from graphics import *
import random
import time

# Global variables
win = None 
board = list() # the board 
occupied = list() #list of occupied and unoccupied squares.
red = None # the two pieces
blue = None
info = None

# Description: draws the 70 sqaures of the board
# Parameters: none
# Return: board -- the list of squares

def draw_board():
    # Nones are added as a border to detect empty spots around the edges.
    board = [None] *12
    for y in range (25, 385, 55):
        board.append(None)
        for x in range (25,550,55):        
            # keeps objects in a list so they can be changed later.
            board.append(Rectangle(Point(x,y),Point(x+50,y+50))) 
            board[-1].setOutline("lightblue")
            board[-1].draw(win)
        board.append(None)
    board = board + [None]* 12
    return board

# Description: move_piece (piece)
# Parameters: piece -- the piece that's being moved.
# Return: None

def move_piece(piece):
    orig = piece.getAnchor ()
    i = get_square()
    sqCenter = board[i].getCenter()
    while (sqCenter.getX() - orig.getX() not in moves 
           or sqCenter.getY() - orig.getY() not in moves
           or occupied[i] != ""): 
        i = get_square()
        sqCenter = board[i].getCenter()
    piece.move(sqCenter.x - orig.x, sqCenter.y - orig.y)   
    occupied[occupied.index(piece)] = "" # remove from previous spot
    occupied[i] = piece # add to new spot  

# Description: breaks the ice of the square clicked if it's not occupied or 
# already broken. Calls getSqaure 
# Parameters: none
# Return: none; it only changes the color of the square on the board.

def break_ice():
    i = get_square ()
    while occupied[i] != "" :
        i = get_square ()
    occupied[i] = "b"
    board[i].setFill("lightblue")
    
# Description: gets a click from the user, determines which square was clicked 
# and returns index of the square. 
# Parameters: none
# Return: sqIndex

def get_square():
    click = win.getMouse()
    while (click.getY() not in range (25,411) or
           click.getX() not in range (25, 576)) :
        click = win.getMouse()
    x = click.x
    y = click.y
    return (y-25) // 55 * 12 + (x-25)//55 + 13

# Description: checks if there are any possible moves
# Parameters: piece -- the piece that has to move
# Return: True or False

def check_move(piece):  
    i = occupied.index(piece)    
    for move in (-1, 1, -11, -12, -13, 11 , 12, 13): #square index
        check = i + move
        if occupied[check] == "":
            return True
    return False

# Description: switches turns
# Parameters: piece -- current piece
# Return: piece -- the other piece

def switch_turns(piece):
    if piece == red:
        piece = blue
    else:
        piece = red
    return piece

# Description: makes one turn. 
# Parameters: piece -- the piece making the move
# Return: none

def turn (piece, play):
    if piece == red:
        if play != 1:
            player  = "Red\'s turn "
        else:
            player = "Red's (player) turn "
    else:
        player = "Blue\'s turn "   
        
    info.setText(player +"to move")
    info.draw (win)
    
    move_piece (piece)
    info.setText(player +'break ice')
    break_ice ()
    info.undraw ()

# Description: moves blue piece (controlled by computer) to a random open 
# adjecent spot
# Parameters: none
# Return: none

def comp_move(): # comp will be blue
    orig_index = occupied.index(blue)
    move_index = orig_index
    while occupied[move_index] is not "":
        move_index = orig_index + random.choice ((-1, 1, -11, -12, -13, 11 , 
                                                  12, 13))
    move_to = board[move_index].getCenter()
    orig = blue.getAnchor ()
        
    blue.move(move_to.getX() - orig.getX(), move_to.getY() - orig.getY())   
    occupied[occupied.index(blue)] = "" # remove from previous spot
    occupied[move_index] = blue # add to new spot    


# Description: computer turn, breaks a random sqaure adjacent to the player.
# Parameters: none
# Return: none

def comp_break ():
    i = 0
    red_index = occupied.index(red)
    move = check_move(red) #in case comp took last free spot
    if move:
        while occupied[i] != "":
            i = red_index + random.choice((-1, 1, -11, -12, -13, 11 , 12, 13))
    else:
        while occupied[i] != "":
            i = random.randint(14, 95) #first 13 and last 13 are None
    occupied[i] = "b"
    board[i].setFill("lightblue")

# Description: One full turn made by the computer
# Parameters: none
# Return: none

def comp_turn ():
    info.setText("Computer's to move")
    info.draw (win)
    time.sleep (1) # 1s makes it possible to read text. 
    comp_move()
    info.setText("Computer's turn to break ice")
    time.sleep (1)
    comp_break()
    info.undraw()

# Description: sets up the board and occupied lists. 
# Parameters: none
# Return: board-- the list of squares. occupied-- list of occupied and 
# unoccupied squares. 

def board_setup():
    board = draw_board () 
    # None for detecting moves around the edges later. 
    occupied = [None]*12
    for i in range (7):
        occupied.append (None)
        for i in range (10):
            occupied.append("")
        occupied.append (None)
    occupied = occupied + [None]*12
    return board, occupied

# Description: shows splash screen
# Parameters: none
# Return: play -- one or two player

def splash ():
    img = Image(Point(300,300), 'images/splash.gif')
    img.draw(win)
    play = 0
    
    while play not in [1,2]:
        y = button_click ()               
        if y in range (300,361):
            play = 1      
        elif y in range (380,441):
            play = 2       
        else:
            how_to = Image(Point(300,300), 'images/howto.gif')
            how_to.draw(win)
            win.getMouse()
            how_to.undraw()
            
    img.undraw()
    
    return play


# Description: determines where the click on the splash screen is.
# Parameters: none
# Return: y - the y coordinate of the click
def button_click ():
    x = 0
    y = 0
    while x not in range (200, 401) or (y not in range (300,361) 
                                       and y not in range (380, 441)
                                       and y not in range (460, 520)):
        click = win.getMouse()
        x = click.getX()
        y = click.getY()    
    return y

# Description: 2 player game. 
# Parameters: none
# Return: none

def icebreaker_2p (piece):
    move = True
    while move:
        turn (piece,2)
        piece = switch_turns(piece)
        move = check_move(piece)      
    if piece == red:
        info.setText("Blue wins \n Click to Close")
    else:
        info.setText("Red wins \n Click to Close")
    info.draw(win)

# Description: 1 player game
# Parameters: none
# Return: none

def icebreaker_1p (piece):
    move = True
    while move:
        if piece == blue:
            comp_turn ()
        else:
            turn (piece,1)          
        piece = switch_turns(piece)
        move = check_move(piece)        
    if piece == red:
        info.setText("Computer Wins \n Click to Close")
    else:
        info.setText("Player wins \n Click to Close")
    info.draw(win)  

# Description: the main function that starts the game
# Parameters: none
# Return: none

def play_game ():
    global win, board, occupied, moves, red, blue, info
    win = GraphWin ("Icebreaker", 600 , 600)  
    Image(Point(300,300), 'images/background.gif').draw(win) #background image
    moves = (-55,0,55)# valid moves as pixels. 
    play = splash ()    
    board, occupied = board_setup() 
    
    # set text style for the entire game
    info = Text(Point(300,525), "")
    info.setSize(15)
    info.setStyle("bold")
    info.setTextColor("lightblue")

    red = Image(Point(50,215),'images/red.gif')
    red.draw(win)
    blue = Image(Point(545,215),'images/blue.gif')
    blue.draw(win)
    occupied[49], occupied[58] = red, blue #initial positions of the pieces   
    piece = [red,blue][random.randint(0,1)] 
    if play == 1:
        icebreaker_1p(piece)
    else:
        icebreaker_2p(piece)  
    win.getMouse()
    win.close()
    
play_game ()